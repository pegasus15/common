package constant

// ACL in this system stands for
//----------------------------------------------------
// Access - this determines what kind of access do you
// have	 this	 reflects  which user group you belong
//----------------------------------------------------
// Control - this determines what control do you. This
// is the  UserModuleType  here  lies  if  if you have
// control	   over		 a		 certain		 route
//----------------------------------------------------
// Level - since we  now determine what control do you
// have the next this to know is what kind of level do
// you have with that  particular control.  This  will
// determine  if  you can CREATE, READ, UPDATE, DELETE

type Module string

type Permission string

const (
	Create       Permission = "CREATE"
	Delete       Permission = "DELETE"
	Get          Permission = "GET"
	GetAll       Permission = "GET_ALL"
	Update       Permission = "UPDATE"
	UpdateStatus Permission = "UPDATE_STATUS"
	Put          Permission = "PUT"
	PutBatch     Permission = "PUT_BATCH"
)

var Permissions = map[Permission]string{
	Create:       "1",
	Delete:       "2",
	Get:          "3",
	GetAll:       "4",
	Update:       "5",
	UpdateStatus: "6",
	Put:          "7",
	PutBatch:     "8",
}
