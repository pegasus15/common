package constant

const (
	MailingUserVerify         = "mailing-user-verify"
	MailingUserForgotPassword = "mailing-user-forgot-password"

	SMSUserVerify         = "sms-user-verify"
	SMSUserForgotPassword = "sms-user-forgot-password"
)
