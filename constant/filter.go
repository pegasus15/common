package constant

type StringFormatter string

const (
	ToLowerStringFormat StringFormatter = "%%%s%%"
)

var StringFormatters = map[StringFormatter]string{
	ToLowerStringFormat: "%%%s%%",
}

type FilterCondition string

const (
	IDEquals FilterCondition = "id = ?"
)

var FilterConditions = map[FilterCondition]string{
	IDEquals: "id = ?",
}
