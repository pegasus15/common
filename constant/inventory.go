package constant

type WarehouseBinInventoryType string
type WarehouseBinInventoryTypeQuantum int64

const (
	Sale          WarehouseBinInventoryType = "SALE"
	Return        WarehouseBinInventoryType = "RETURN"
	AdjustmentIn  WarehouseBinInventoryType = "STOCK_ADJUSTMENT_IN"
	AdjustmentOut WarehouseBinInventoryType = "STOCK_ADJUSTMENT_OUT"
	InventoryIn   WarehouseBinInventoryType = "INVENTORY_IN"
	InventoryOut  WarehouseBinInventoryType = "INVENTORY_OUT"
	Assembly      WarehouseBinInventoryType = "ASSEMBLY"
	Disassembly   WarehouseBinInventoryType = "DISASSEMBLY"
)

const (
	Add      WarehouseBinInventoryTypeQuantum = 1
	Subtract WarehouseBinInventoryTypeQuantum = -1
)

var WarehouseBinInventoryTypes = map[string]WarehouseBinInventoryType{
	"SALE":                 Sale,
	"RETURN":               Return,
	"STOCK_ADJUSTMENT_IN":  AdjustmentIn,
	"STOCK_ADJUSTMENT_OUT": AdjustmentOut,
	"INVENTORY_IN":         InventoryIn,
	"INVENTORY_OUT":        InventoryOut,
	"ASSEMBLY":             Assembly,
	"DISASSEMBLY":          Disassembly,
}

var WarehouseBinInventoryTypeQuantums = map[WarehouseBinInventoryType]WarehouseBinInventoryTypeQuantum{
	Sale:          Subtract,
	Return:        Add,
	AdjustmentIn:  Add,
	AdjustmentOut: Subtract,
	InventoryIn:   Add,
	InventoryOut:  Subtract,
}
