package constant

type TimeLogType string

const (
	In  TimeLogType = "IN"
	Out TimeLogType = "OUT"
)

var TimeLogTypes = map[string]TimeLogType{
	"IN":  In,
	"OUT": Out,
}
