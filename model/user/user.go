package user

import (
	"github.com/dgrijalva/jwt-go"
)

type UserVerifyCustomClaims struct {
	Username  string `json:"username"`
	UserGroup string `json:"userGroup"`
	jwt.StandardClaims
}

type UserForgotPasswordCustomClaims struct {
	Username  string `json:"username"`
	UserGroup string `json:"userGroup"`
	jwt.StandardClaims
}

type UserRefreshTokenCustomClaims struct {
	ID string `json:"id"`
	jwt.StandardClaims
}

type UserAccessTokenCustomClaims struct {
	ID           string            `json:"id"`
	Username     string            `json:"username"`
	FirstName    string            `json:"firstName"`
	MiddleName   string            `json:"middleName"`
	LastName     string            `json:"lastName"`
	UserIDNumber string            `json:"userIDNumber"`
	Email        string            `json:"email"`
	ACL          map[string]string `json:"acl"`
	jwt.StandardClaims
}
