package route

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

var AllowedHeaders = []string{
	"Accept",
	"content-type",
	"Content-Length",
	"Accept-Encoding",
	"Authorization",
	"Bearer",
	"File-Name",
	"File-Size",
	"File-Type",
	"Mime-Type",
	"File-Extension",
}

var AllowedMethods = []string{
	http.MethodOptions,
	http.MethodGet,
	http.MethodPost,
	http.MethodPatch,
	http.MethodDelete,
	http.MethodPut,
}

var AllowedOrigins = []string{"*"}

func ProvideRouter() *gin.Engine {
	router := gin.New()
	router.SetTrustedProxies([]string{"http://124.105.183.235"})

	router.Use(cors.New(cors.Config{
		AllowCredentials: true,
		AllowOrigins:     AllowedOrigins,
		AllowMethods:     AllowedMethods,
		AllowHeaders:     AllowedHeaders,
	}))

	router.Use(gin.LoggerWithFormatter(routerLogger))
	router.Use(gin.Recovery())

	router.Use(responseHeader)

	return router
}

func responseHeader(ctx *gin.Context) {
	ctx.Header("Access-Control-Request-Headers", strings.Join(AllowedOrigins, ","))
	ctx.Header("Access-Control-Request-Method", strings.Join(AllowedMethods, ","))
	ctx.Header("Access-Control-Expose-Headers", strings.Join(AllowedHeaders, ","))
	ctx.Next()
}

func routerLogger(param gin.LogFormatterParams) string {
	return fmt.Sprintf("\nClient IP: %s\nTimestamp: [%s]\nMethod: %s\nPath: %s\nProto: %s\nStatus: %d\nLatency: %s\nUserAgent: %s\nError: %s\n\n",
		param.ClientIP,
		param.TimeStamp.Format(time.RFC1123),
		param.Method,
		param.Path,
		param.Request.Proto,
		param.StatusCode,
		param.Latency,
		param.Request.UserAgent(),
		param.ErrorMessage,
	)
}
