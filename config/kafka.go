package config

type KafkaConfig struct {
	Host string `envconfig:"KAFKA_HOST"`
}
