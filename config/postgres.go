package config

import (
	"github.com/kelseyhightower/envconfig"
)

type PostgresConfig struct {
	Host     string `envconfig:"POSTGRES_HOST" required:"true"`
	Port     int    `envconfig:"POSTGRES_PORT" required:"true"`
	Name     string `envconfig:"POSTGRES_DB" required:"true"`
	User     string `envconfig:"POSTGRES_USER" required:"true"`
	Password string `envconfig:"POSTGRES_PASSWORD" required:"true"`
	SSLMode  bool   `envconfig:"POSTGRES_SSL_MODE" required:"true"`
	TimeZone string `envconfig:"POSTGRES_TIME_ZONE" required:"true"`
}

func ProvidePostgresConfig(appConfig *AppConfig) *PostgresConfig {
	var conf PostgresConfig
	envconfig.MustProcess(string(appConfig.AppMode), &conf)
	return &conf
}
